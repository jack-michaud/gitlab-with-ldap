
FROM gitlab/gitlab-ce:9.5.10-ce.0

WORKDIR /etc/gitlab

RUN echo "gitlab_rails['ldap_enabled'] = true" >> gitlab.rb
RUN echo "gitlab_rails['ldap_servers'] = {" >> gitlab.rb
RUN echo "'main': => {" >> gitlab.rb
RUN echo "  'label': => '${ldap_label}'," >> gitlab.rb
RUN echo "  'host': => '${ldap_host}'," >> gitlab.rb
RUN echo "  'port': => '${ldap_port}'," >> gitlab.rb
RUN echo "  'uid': => '${ldap_uid}'," >> gitlab.rb
RUN echo "  'encryption': => '${ldap_encryption}'," >> gitlab.rb
RUN echo "  'verify_certificates': => true," >> gitlab.rb
RUN echo "  'bind_dn': => '${ldap_bind_dn}'," >> gitlab.rb
RUN echo "  'password': => '${ldap_password}'," >> gitlab.rb
RUN echo "  'active_directory': => '${active_directory}'," >> gitlab.rb
RUN echo "  'base': => '${ldap_base}'," >> gitlab.rb
RUN echo "  'group_base': => '${ldap_group_base}'," >> gitlab.rb
RUN echo "  'admin_group': => '${ldap_admin_group}'" >> gitlab.rb
RUN echo " }" >> gitlab.rb
RUN echo "}" >> gitlab.rb


